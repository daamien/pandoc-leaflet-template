<img src="examples/images/leaf.png" align="right" height="110"/>

Leaflet Template for Pandoc
===============================================================================

A pandoc LaTeX template to create a 3-fold brochure with a simple markdown file.

Preview
--------------------------------------------------------------------------------

The template creates a document of six small pages in portrait orientation, 
arranged physically on 2 “normal-size” landscape pages. 

| **Front Page**   | **Back Page**      |
| ---------------- | ------------------ |      
| [![A leaflet example](examples/advanced.pdf-page-1.png)](examples/advanced.pdf) | [![A leaflet example](examples/advanced.pdf-page-2.png)](examples/advanced.pdf)  | 

Markdown source file: [examples/advanced.md](examples/advanced.md) 

Install
--------------------------------------------------------------------------------

1.  [Install pandoc] and [install LaTeX] and [install xelatex]. You should probably [install texlive] too.
2.  Download the latest version of the Leaflet template from [the release page].
3.  Extract the downloaded archive and open the folder.
4.  Move the template `leaflet.latex` to your pandoc templates folder. The location of the templates folder depends on your operating system:
      - Unix, Linux, macOS: usually `$HOME/.pandoc/templates/`
      - Windows: `C:\Users\USERNAME\AppData\Roaming\pandoc\templates\`

If there are no folders called `templates` or `.pandoc`, you need to create them and put the template `leaflet.latex` inside. You can find the default user data directory on your system by looking at the output of `pandoc --version`.

[Install pandoc]: https://pandoc.org/installing.html
[install LaTeX]: https://en.wikibooks.org/wiki/LaTeX/Installation#Distributions
[install xelatex]: http://xetex.sourceforge.net/
[install texlive]: http://www.tug.org/texlive/
[the release page]: https://gitlab.com/daamien/pandoc-leaflet-template/-/releases


Usage
--------------------------------------------------------------------------------

``` bash
$ pandoc foo.md --pdf-engine=xelatex --template leaflet -o foo.pdf
```

Variables
--------------------------------------------------------------------------------

The template is designed to work out-of-the-box without configuration. However
the [pandoc template variables from pandoc] are supported and you can use the 
variables below to control the appearance of the document: 

* `leaflet-background-text` (default: none) \
  Adds a background text accross the 3 inside pages of the brochure (pages 2, 3 and 4).

* `leaflet-background-text-color` (default: gray) \
  Color of the background text.

* `leaflet-heading1-color` ( default: "444444" ) \
   Color the level 1 titles.

* `leaflet-heading2-color` ( default: "444444" ) \
   Color the level 2 titles.

* `leaflet-logo` (default: none) \
  Adds an image to the first page

* `leaflet-logo-width` (default: 100) \
  Size of the logo (in `pt`).

* `leaflet-no-cutlines` (default: none) \
  If the variable is defined (including `false` or `off`), the vertical cut lines will be removed.

* `leaflet-no-tumble` (default: none) \
  If the variable is defined (including `false` or `off`), the back page is not turned upside down. 

* `leaflet-page1-background-color` (default: none) \
  Adds a background color to page 1.

* `leaflet-page1-background-image`  (default: none) \
  The path of the a background image of page 1. The image is scaled to cover the entire page. 
  This variable overrides `leaflet-page1-background-color`.

The background color and image variable is available for all 6 pages with `leaflet-page2-background-color`,
`leaflet-page3-background-image`, etc.

### Notes:

* Check out the [advanced example] to see the variables in action !

* The **image paths are always relative to where pandoc is executed**. The option `--resource-path` has no effect.

* The **color values must be given as an HTML hex color** like `D8DE2C` without the leading number sign (`#`). When specifying the color in YAML, it is advisable to enclose it in quotes like so `leaflet-page4-background-color: "D8DE2C"` to avoid the truncation of the color (e.g. `000000` becoming `0`).

[advanced example]: examples/advanced.md
[pandoc template variables from pandoc]: https://pandoc.org/MANUAL.html#variables-for-latex


Layout
--------------------------------------------------------------------------------

The default "print format" is A4. The "folded format" is 10x21 cm.

You can use the `papersize` variable to change it. 

The order of pages is 

| **Front Page**   | **Back Page**      |
| ---------------- | ------------------ |      
| [![front layout](examples/layout.pdf-page-1.png)](examples/layout.pdf) | [![back layout](examples/layout.pdf-page-2.png)](examples/layout.pdf)  | 


Changelog
--------------------------------------------------------------------------------

### Version 2.0 : WIP FIXME

### Version 1.0 : january 1st, 2020

* Initial version
* Supports background color and image on each page
* Disable cut lines with `leaflet-no-cutlines` 
* Disable tumble with `leaflet-no-tumble`
* Basic and Advanced examples, Makefile and CI

Authors
--------------------------------------------------------------------------------

This template is maintained by: 

* Damien Clochard


Licence
--------------------------------------------------------------------------------

Copyright (c) 2020, Damien Clochard
All rigths reserved.
This project is licensed under the [BSD 3-Clause "New" or "Revised" License].

[BSD 3-Clause "New" or "Revised" License]: https://choosealicense.com/licenses/bsd-3-clause/


Credits
--------------------------------------------------------------------------------

This project relies on the following documents:

* The pandoc [default latex template]
* The awesome [eisvogel template]
* The latex [leaflet class]
* The [leaflet class samples]
* The [leaflet class manual]

[default latex template]: https://github.com/jgm/pandoc-templates/blob/master/default.latex
[eisvogel template]: https://github.com/Wandmalfarbe/pandoc-latex-template
[leaflet class]: https://ctan.org/tex-archive/macros/latex/contrib/leaflet
[leaflet class samples]: https://www.dickimaw-books.com/latex/admin/html/leafletcls.shtml#sample-leaflet
[leaflet class manual]: https://github.com/rolfn/leaflet/blob/master/leaflet-manual.tex

