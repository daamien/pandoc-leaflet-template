
##
## Variables
##

# the source file
TEMPLATE?=leaflet.latex

# path to pandoc
PANDOC?=pandoc

# path to pdftoppm (poppler)
PDFTOPPM?=pdftoppm

# use the pandocker image instead of local pandoc
ifneq ($(PANDOCKER),)
	PANDOC=docker run --rm  -v `pwd`:/pandoc dalibo/pandocker:$(PANDOCKER)
	PDFTOPPM=docker run --rm  -v `pwd`:/pandoc --entrypoint pdftoppm dalibo/pandocker:$(PANDOCKER)
endif

##
## Source and objects
##
EXAMPLES_MD  := $(shell find examples -name '*.md')
EXAMPLES_TEX := $(patsubst %.md, %.tex, $(EXAMPLES_MD) )
EXAMPLES_PDF := $(patsubst %.md, %.pdf, $(EXAMPLES_MD) )
EXAMPLES_PNG := $(patsubst %.md, %.pdf-page-1.png, $(EXAMPLES_MD) )
EXAMPLES_PNG += $(patsubst %.md, %.pdf-page-2.png, $(EXAMPLES_MD) )


##
## Targets
##

all: examples

.PHONY: examples $(TEMPLATE)
examples: $(EXAMPLES_PDF) $(EXAMPLES_PNG)

.PHONY: tex
tex: $(EXAMPLES_TEX)

%.tex: %.md
	$(PANDOC) --template $(TEMPLATE) $^ -o $@

%.pdf: %.md
	$(PANDOC) --pdf-engine=xelatex --template $(TEMPLATE) $^ -o $@

%.pdf-page-1.png %.pdf-page-2.png: %.pdf
	$(PDFTOPPM) $^ -png $^-page
