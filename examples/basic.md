
<!-- page 1 -->

# Basic Leaflet with Pandoc


![](./examples/images/undraw_eco_conscious.png)


**Create a simple 3-fold brochure from a regular markdown file**


<!-- Jump to page 2 -->
\newpage

# This is page 2

Vinaque sanguine metuenti cuiquam Alcyone fixus

## Aesculeae domus vincemur et Veneris adsuetus lapsum

Lorem markdownum Letoia, et alios: figurae flectentem annis aliquid Peneosque ab
sse, obstat gravitate. Obscura atque coniuge, per de coniunx, sibi **medias
commentaque virgine** anima tamen comitemque petis, sed. In Amphion vestros
hamos ire arceor mandere spicula, in licet aliquando.

### Et harum quidem rerum

Nam libero tempore,
cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod
maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor
repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum
necessitatibus saepe eveniet ut et voluptates. 

---

_Itaque earum rerum hic tenetur a sapiente delectus, ut aut
reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus
asperiores repellat._

---


<!-- Jump to page 3 -->
\newpage

```java
public class Example implements LoremIpsum {
	public static void main(String[] args) {
		if(args.length < 2) {
			System.out.println("Lorem ipsum dolor sit amet");
		}
	} // Obscura atque coniuge, per de coniunx
}
```

Porrigitur et Pallas nuper longusque cratere habuisse sepulcro pectore fertur.
Laudat ille auditi; vertitur iura tum nepotis causa; motus. Diva virtus! Acrota
destruitis vos iubet quo et classis excessere Scyrumve spiro subitusque mente
Pirithoi abstulit, lapides.


<!-- Jump to page 4 -->
\newpage

## Page 4 = Lydia caelo recenti haerebat lacerum ratae at

Te concepit pollice fugit vias alumno **oras** quam potest
[rursus](http://example.com#rursus) optat. Non evadere orbem equorum, spatiis,
vel pede inter si.

1. De neque iura aquis
2. Frangitur gaudia mihi eo umor terrae quos
3. Recens diffudit ille tantum

> "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium 
> doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore 
> veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
> voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
>  magni dolores eos qui ratione voluptatem sequi nesciunt."

<!-- Jump to page 5 -->
\newpage

# Ramen ex Parlano 

Et nepotes poterat, se qui. Euntem ego pater desuetaque aethera Maeandri, et
[Dardanio geminaque](http://example.com#Dardanio_geminaque) cernit. 

## Quis autem vel eum iure reprehenderit 

\begin{equation}\label{eq:neighbor-propability}
    p_{ij}(t) = \frac{\ell_j(t) - \ell_i(t)}{\sum_{k \in N_i(t)}^{} \ell_k(t) - \ell_i(t)}
\end{equation}

### Tamen condeturque saxa 

Lassaque poenas
nec, manifesta $\pi r^2$ mirantia captivarum prohibebant scelerato gradus
unusque dura.

---

_Pallorque num et ferarum promittis inveni lilia iuvencae
adessent arbor. Florente perque at condeturque saxa et 
ferarum promittis tendebat. Armos nisi obortas refugit me._

---

Mollit anim id est laborum.

<!-- Jump to page 6 (back cover) -->
\newpage

### Page 6 : gaudia mihi eo umor terrae

| Tables   |      Are      |  Cool |
|----------|:-------------:|------:|
| col 1 is |  left-aligned | $1600 |
| col 2 is |    centered   |   $12 |
| col 3 is | right-aligned |    $1 |

: This is a table

- Permulcens flebile simul
- Iura tum nepotis causa motus diva virtus Acrota. Tamen condeturque saxa Pallorque num et ferarum promittis inveni lilia iuvencae adessent arbor. Florente perque at ire arcum.
